package com.encryption.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class EncryptionUtil {

    private static final String RSA_PADDING = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
    private static final String AES_PADDING = "AES/CBC/PKCS5Padding";
    private static final String CERTIFICATE_TYPE = "RSA";
    private static final String AES = "AES";
    private static final String RSA_ENCODING = "UTF-8";
    private static final int RSA_KEY_BYTES = 3072;

    /**
     * This method is used to apply the encryption to a value using a public key created
     * with padding RSA.
     * */
    public static String rsaEncrypt(String value, String publicKey){
        log.info("[Start][EncryptionUtil][rsaEncrypt]");
        byte [] publicKeyBytes = Base64.decodeBase64(publicKey);
        String encryptedValue = "";
        try {
            X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            Cipher cipher = Cipher.getInstance(RSA_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, KeyFactory.getInstance(CERTIFICATE_TYPE).generatePublic(encodedKeySpec),
                new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT));
            encryptedValue = Base64.encodeBase64String(cipher.doFinal(value.getBytes(RSA_ENCODING)));
        }catch (Exception e){
            log.error("[Error][EncryptionUtil][rsaEncrypt]Error: {}", e.getMessage());
        }
        log.info("[End][EncryptionUtil][rsaEncrypt]");
        return encryptedValue;
    }

    public static String rsaDecrypt(String value, String privateKey){
        log.info("[Start][EncryptionUtil][rsaDecrypt]");
        byte [] privateKeyBytes = Base64.decodeBase64(privateKey);
        String decryptedValue = "";
        try{
            PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            Cipher cipher = Cipher.getInstance(RSA_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance(CERTIFICATE_TYPE).generatePrivate(encodedKeySpec),
                new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT));
            decryptedValue = new String(cipher.doFinal(Base64.decodeBase64(value)), RSA_ENCODING);
        }catch (Exception e){
            log.error("[Error][EncryptionUtil][rsaDecrypt]Error: {}", e.getMessage());
        }
        log.info("[End][EncryptionUtil][rsaDecrypt]");
        return decryptedValue;
    }

    public static Map<String, String> rsaKeyGeneration(){
        log.info("[Start][EncryptionUtil][rsaKeyGeneration]");
        Map<String, String> keys = new HashMap<>();
        try {
            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance(CERTIFICATE_TYPE);
            keyGenerator.initialize(RSA_KEY_BYTES);

            KeyPair keyPair = keyGenerator.generateKeyPair();

            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();

            keys.put("private", com.sun.org.apache.xml.internal.security.utils.Base64.encode(privateKey.getEncoded()));
            keys.put("public", com.sun.org.apache.xml.internal.security.utils.Base64.encode(publicKey.getEncoded()));
        }catch (Exception e){
            log.error("[Error][EncryptionUtil][rsaKeyGeneration]Error: {}", e.getMessage());
        }
        log.info("[End][EncryptionUtil][rsaKeyGeneration]");
        return keys;
    }

    public static String aesEncrypt(String privateKey, String key, String iv){
        log.info("[Start][EncryptionUtil][aesEncrypt]");
        String encryptedValue = "";
        try{
            final byte [] secretKey = DatatypeConverter.parseHexBinary(key);
            final byte [] initVector = DatatypeConverter.parseHexBinary(iv);

            final Cipher cipher = Cipher.getInstance(AES_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(secretKey, AES), new IvParameterSpec(initVector, 0, cipher.getBlockSize()));

            encryptedValue = com.sun.org.apache.xml.internal.security.utils.Base64.encode(cipher.doFinal(privateKey.getBytes(RSA_ENCODING)));
        }catch (Exception e){
            log.error("[Error][EncryptionUtil][aesEncrypt] Error: {}", e.getMessage());
        }
        log.info("[End][EncryptionUtil][aesEncrypt]");
        return encryptedValue;
    }

    public static String aesDecrypt(String encryptedValue, String key, String iv){
        log.info("[Start][EncryptionUtil][aesDecrypt]");
        String decryptedValue = "";
        try{
            final byte [] secretKey = DatatypeConverter.parseHexBinary(key);
            final byte [] initVector = DatatypeConverter.parseHexBinary(iv);

            final Cipher cipher = Cipher.getInstance(AES_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(secretKey, AES), new IvParameterSpec(initVector, 0, cipher.getBlockSize()));

            byte [] decodeValue = com.sun.org.apache.xml.internal.security.utils.Base64.decode(encryptedValue);
            decryptedValue = new String(cipher.doFinal(decodeValue));
        }catch (Exception e){
            log.error("[Error][EncryptionUtil][aesDecrypt] Error: {}", e.getMessage());
        }
        log.info("[End][EncryptionUtil][aesDecrypt]");
        return decryptedValue;
    }
}
