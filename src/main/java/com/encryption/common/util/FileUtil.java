package com.encryption.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;

@Slf4j
public class FileUtil {

    /**
     * Write the key value to a file
     * @param value - This is the value to be written on the file
     * @param fileName - This is the path and file name
     * */
    public static void writeKey (String value, String fileName){
        log.trace("[Start][FileUtil][writeKey]");
        try(FileOutputStream fileOutputStream = new FileOutputStream(fileName)){
            fileOutputStream.write(value.getBytes());
        }catch (Exception e){
            log.error("[Error][FileUtil][writeKey] Error while writing the file {} : {}", fileName, e);
        }
        log.trace("[End][FileUtil][writeKey]");
    }
}
