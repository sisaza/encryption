package com.encryption;

import com.encryption.common.util.CardUtil;
import com.encryption.common.util.EncryptionUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptCardTest {

    private static final Logger logger = LoggerFactory.getLogger(EncryptCardTest.class);

    private static final Long MAX_CARD_VALUE = Long.valueOf("9999999999");
    private static final String PUBLIC_KEY = "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAgIBfju9XO0YjctTyrut862Dqb2EgniHT8oIFPwO3i69PozpAmuzGTuNGJgHPhTPWKCz0Y44KQcdsDcB0dekPPTab4yOSr9e3KgytbCGFfqopboewa3rkp+aq5uT/1snk2pzBClv8pUHZh4yfhKlJpObY6LZQ+ttGzbMAVg+wIlfwVQ01j/X+w1KCycuSdB973Q1vv3pkTW9IarW/neZyGMyuToOKbdvhtT1N0TynMDjguTPZCVEEJNNd81xCkZRuty9yZLhR3vK/RtNFKYXlyCZEwetsvAMmcRWaAZgX1mFkxwgMPjvjM2MIbjqlEdxjzRrv9TdQ45weasWU3C9UlE/lGd1S1vpVGt01mIr9FDDCef33xlOejcUhyIBO+b8pD/zocVcGN073VAIwkcA0XTOiSJhdeUa24Jwhz9y3x09NicjIzPxUibpQWaeBPDzDxbfLQDztFh80nzcLFBNRWgfQhso3FWCOpWHZ26Z7QILmXzeUcBqBMiETxPYDNdbVAgMBAAE=";
    private static final String PRIVATE_KEY = "MIIG/AIBADANBgkqhkiG9w0BAQEFAASCBuYwggbiAgEAAoIBgQCAgF+O71c7RiNy1PKu63zrYOpvYSCeIdPyggU/A7eLr0+jOkCa7MZO40YmAc+FM9YoLPRjjgpBx2wNwHR16Q89NpvjI5Kv17cqDK1sIYV+qiluh7BreuSn5qrm5P/WyeTanMEKW/ylQdmHjJ+EqUmk5tjotlD620bNswBWD7AiV/BVDTWP9f7DUoLJy5J0H3vdDW+/emRNb0hqtb+d5nIYzK5Og4pt2+G1PU3RPKcwOOC5M9kJUQQk013zXEKRlG63L3JkuFHe8r9G00UpheXIJkTB62y8AyZxFZoBmBfWYWTHCAw+O+MzYwhuOqUR3GPNGu/1N1DjnB5qxZTcL1SUT+UZ3VLW+lUa3TWYiv0UMMJ5/ffGU56NxSHIgE75vykP/OhxVwY3TvdUAjCRwDRdM6JImF15RrbgnCHP3LfHT02JyMjM/FSJulBZp4E8PMPFt8tAPO0WHzSfNwsUE1FaB9CGyjcVYI6lYdnbpntAguZfN5RwGoEyIRPE9gM11tUCAwEAAQKCAYAMhXXKRcQoQHNf0yQ6qzOm8/CkHcDB5+XxMX41X7VY5SJLc3esIPpJ4ZeKSI7iGeT6UTcXJFYXhskepewzORyFCHc0MNkwTULJ1CZp7PJHH2meHZaxQm5Zdf6dNR9A3LdyHegpjEEYEUyTCr7YfG5ypRPpySom54sErDaSLEErd4oxn3Hir0O94KJh63Y/3nmBVrfW3qQ7b2aND1y2lCf2BqvNdr/caJqbwHd+tfxMlJ5BLa4uXDidJgN3nTvXV7E2pEPg6iTRVTYT7CXX/NTUsFeBNuypzVyJPTQhTSqeW7UjhJm6fZc9GuIF0Q1Y7+ktlvi7+VdWY4mtna8WNd6FpJ1CpYLpKTPFJE8t6iMg04xcbJFbGK+OkPt8Ifrzz1x4ygsG5j8skZLlkWju7W9PUheQM9xVxBEJvvoGh+qBaGwON8g4D42CCndbvrhoPsZCUqqqn06Dcyg/Mp1+Gpc9hGhoPgNYKgYx0uf34uCtRAU4MzbnSz4plubuKz+/ulkCgcEAxxUprb6pwjDDf1em3Z05DymdRyGK822X6rxLD2B/6sob97uEZM+o1UPLC0cptF+SsQYj3erwYVfr+l9w2qg9NL6G+UGuFo6DPaWDuZv+e2A29876coHiOHfV3gq8FWT4dGkEf4QLIZzDhfhNC2CrK6P4tKNkwnzYCk2Y7A+GMOWhegUxn/XOx7qRmAhv5SulL0gwt8dC0eU+0AQG8Pz0DXcv1kmN0Haz0AD80T9tQH/tlC9z4EOOw5/heN8odAQHAoHBAKU9YUgC979aLhV8B1IisFmH/cQhwlNd5sYZRgbFL7YZ1IGelI2vL2+Ya3WWwKMcCl/72YXG6Z6dB1awMczk8oMewsbw40LxMb13BT6Hr9N50+hzOkYwE28EQTU0RH0nbXRudLQeQzW8fHpnReL2A/3zxBqstkN68hY0ujeq1kt95del0/SCz8amdC9MUhVGk4UAM0uwEUL08PwQHLjaBMf4DpV3dSXpMvPNB0LkbSmYPSyB8xUBTSgl76DoWyivQwKBwEx4rpfUwzSasp25t5ktCXvvikCGe+9hy5cZWOPBN6GinPeeCg8HTV0OO3JLlZN5mslxCFn4IvnhYf28p4b2VPxrq0EXn51+hJ8rctvbNfeCT9sEoGNXzOPP9FDkYrb9IArnRBkPSY9RvjE53a5fJaEPzTrmAJBMkFf00qbql6s0qcBLOnKtYw6Jdd+mbgTjrQHqgvw1CE/D9UrGITx0jfKWM8PmYRkQX6Wom3RtHrRF76aaS7J7gN1YDq31Wn6mBQKBwD0LLuwehI4Bb7LqLf4u6H062wng+tw3fXDDl3Ya1KRuaG42OmUZSentIg2/0oyvaDnLXbGpKVc6ZaKl1s/96IV+ELDT/vEVD64R0TMnh1IV2K4wG2AglJ1XsgtIsuCVdz8jKyP7ZhLs/gNuMYhGIHVp35GHKpJFUjfX5M954k7jRMzk6yeSR9X4WkW66TwI5ailyU11zvqwaHKGNUYo7Mk/H4wiJHoL65HbtYoFvzo1jqvpmfaxk2J4mU0Vac/e5wKBwHCA9dR9Kr+lZ+rAu0XhjwhTxAycQKHQcxEuWldqf4s8ZaGlv5oe798yY3IJT2OOk5FG3FLN/U2HO69alEs4lTPqY+KNaYUKwY0rEh5IOzG06ABIfEYxFTWL9rAo576ToivsWKJ/TMdwYtW8rx4rmUJMvzvVGMkWZTsXdJdt36WWlZ1d9GTubqk7gax0dTtlDs4O0cMZFGYKtJVTUoaQo0iNzGPlPEys992eSNyml6MS0MzqvXg6wYIeXaqjswxGcw==";
    private static final String KEY = "f743a7c51ffc9537450e6aa97c77ef17615e9d623b301ec772c4c6c539715f01";
    private static final String IV = "5bf474a1a7c3d538ac8004b5cc29291f";
    private static final String ENCRYPTED_CARD = "JupnAEdSnJUZPNJYUa6/J3OaFHMkXDFJXAAtJfLE1EQPwntTWtjSIau89PsNy5LRuiTasOu9cohGWtBZsq+MGOVfV4jS0FJkkDYXUz6pU0JFatC5z9ijkY7fy0mFGoklbHHVdL+j+3GPI6B1b43i0+oEvIbCt7p+F8WdUBGyaqW+CIKHOvYFzxv+SAbFJkMXtH1i3vtLwRl4Xd9uo65DxxkIFT4a4kNbnlJrkE5YT3sehv1/9jC8PAwhu7CLgZTvGKt1FHrGl/ENvxRbBOg5ZF/XExx6+4yvno3AuwmITlm5KuwZp/Y6HWz7neJnF5LAYGUSPmeSoyZltbqBbifm8g==";

    private static final String DES_KEY = "60DFBD94E25F108B1CB840B82D940982D07FE4ED501A04DF";


    @Test
    public void encryptCardTest(){
        /**
         * Para encriptar una fecha se debe de seguir el siguiente patron: mes representado con dos dígitos MM,
         * sigue el valor 0 y finalmente se pone el valor del año representado con dos dígitos YY, como este debe
         * ser un valor Hexadecimal se completa con FFFFFFFFFFF,
         * Ejemplo: si la fecha de expiración es 11/23 el valor a encriptar es: 11023FFFFFFFFFFF.
         *
         * Para encriptar las tarjetas se debe de tener en cuenta lo siguiente, estas deben de ser de 16 dígitos,
         * si se quiere encriptar mas de una tarjeta estas deben de estar separadas por comas y sin espacios
         * Ejemplo: 5587842453817293,5546272453817293,5512762453817293
         * */
        String value = "465956";

        int numberOfCards = 0;

        String cardToValidate = "";
        Long cardNumber = Long.valueOf(30);
        while (cardNumber < MAX_CARD_VALUE ){
            String card = StringUtils.leftPad(cardNumber.toString(), 10, '2');
            cardToValidate = value+ card;

            if(CardUtil.isValidCreditCardNumber(cardToValidate)){
                String encrypted = EncryptionUtil.rsaEncrypt(cardToValidate, PUBLIC_KEY);
                logger.info("[EncryptCardTest][encryptCardTest]Encrypted card : {}", encrypted);

                if(numberOfCards == 20){
                    break;
                }else{
                    numberOfCards ++;
                }
                String decryptedCard = EncryptionUtil.rsaDecrypt(encrypted, PRIVATE_KEY);

                Assert.assertTrue(cardToValidate.equals(decryptedCard));
            }
            cardNumber++;
        }
    }

    @Test
    public void aesEncryptionTest(){
        String encryptedKey = EncryptionUtil.aesEncrypt(PRIVATE_KEY, KEY, IV);

        logger.info("[EncryptCardTest][aesEncryptionTest] Encrypted Value = {}", encryptedKey);

        String decryptedKey = EncryptionUtil.aesDecrypt(encryptedKey, KEY, IV);

        Assert.assertTrue(PRIVATE_KEY.equals(decryptedKey));
    }

    @Test
    public void decryptCardValue (){

        String decryptedCard = EncryptionUtil.rsaDecrypt(ENCRYPTED_CARD, PRIVATE_KEY);

        logger.info("[EncryptionUtil][decryptCardValue]decrypted card = {}", decryptedCard);

        Assert.assertTrue(!decryptedCard.isEmpty());

    }

    @Test
    public void encryptDreKey(){
        logger.info("[Start][EncryptCardTest][encryptDreKey]");
        String encryptedKey = EncryptionUtil.aesEncrypt(DES_KEY, KEY, IV);
        logger.info("[EncryptCardTest][encryptDreKey]Encrypted key: {}", encryptedKey);

        String decryptedKey = EncryptionUtil.aesDecrypt(encryptedKey, KEY, IV);
        Assert.assertTrue(DES_KEY.equals(decryptedKey));

    }
}
