package com.encryption;

import com.encryption.common.util.CardUtil;
import com.encryption.common.util.EncryptionUtil;
import com.encryption.common.util.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
@Slf4j
public class KeyCreationTest {

    private static final Long MAX_CARD_VALUE = Long.valueOf("9999999999");
    private static final String FP_PRIVATE_ENCRYPTED_KEY_PATH = "/Volumes/Personal/YellowPepper/config-files/aval/keystore/tv.yellowpepper.first.purchase.popular.com.pem.enc";
    private static final String FP_PUBLIC_KEY_PATH = "/Volumes/Personal/YellowPepper/config-files/aval/keystore/tv.yellowpepper.first.purchase.popular.com.public.pem";
    private static final String KEK_KEY = "f788e5760ee1eeedfd9e736ae33e288c272e0d226c7c10cbd1a9d4eddfc3eec2";
    private static final String KEK_IV = "4973e6e583573ee156bf8fbe7a5b52c4";

    @Test
    public void encryptAndDecryptValue(){
        log.info("[Start][KeyCreationTest][encryptAndDecryptValue]");
        Map<String, String> fpKeys = EncryptionUtil.rsaKeyGeneration();

        String privateEncryptedKey = EncryptionUtil.aesEncrypt(fpKeys.get("private"), KEK_KEY, KEK_IV);

        FileUtil.writeKey(privateEncryptedKey, FP_PRIVATE_ENCRYPTED_KEY_PATH);
        FileUtil.writeKey(fpKeys.get("public"), FP_PUBLIC_KEY_PATH);

        generateCards(fpKeys.get("public"), fpKeys.get("private"));
    }
    
    private void generateCards(String publicKey, String privateKey){

        log.info("Public key: {}", publicKey);
        log.info("Privaet key: {}", privateKey);

        String value = "465956";

        int numberOfCards = 0;

        String cardToValidate = "";
        Long cardNumber = Long.valueOf(30);
        while (cardNumber < MAX_CARD_VALUE ){
            String card = StringUtils.leftPad(cardNumber.toString(), 10, '2');
            cardToValidate = value+ card;

            if(CardUtil.isValidCreditCardNumber(cardToValidate)){
                String encrypted = EncryptionUtil.rsaEncrypt(cardToValidate, publicKey);
                log.info("-----------------------------------------------------------------------");
                System.out.println("[KeyCreationTest][generateCards]Encrypted card : "+ encrypted);

                String decryptedCard = EncryptionUtil.rsaDecrypt(encrypted, privateKey);
                log.info("[KeyCreationTest][generateCards]Decrypted card : {}", decryptedCard);
                log.info("-----------------------------------------------------------------------");
                Assert.assertTrue(cardToValidate.equals(decryptedCard));

                if(numberOfCards == 20){
                    break;
                }else{
                    numberOfCards ++;
                }

            }
            cardNumber++;
        }
    }
}
